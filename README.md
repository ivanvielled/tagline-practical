##Tagline Practical Exam made using PHP Laravel, MySQL, HTML, CSS, Bootstrap, Jquery
##Created by Ivanvielle B. Dupaya

##Instructions to run the web application

##Run composer install after cloning the project folder
##Rename .env.example to .env
##create MySQL database locally named tagline_practical
##Run php artisan migrate /OR/ php artisan migrate:refresh
##Run php artisan serve then open your browser to http://localhost:8000

-I intentionally left the datatables empty so that examiners can test the application as they run through it.

##Main Functionalities:

##Palindrome Checker
-displayed on the first tab on the welcome page
-checks if the word is palindrome then saves it on the database then displayed on the datatable with the result

##Number sorter
-displayed on the second tab on the welcome page
-upon clicking the sort button runs the sort function which displays the sorted list of the number array in descending order

##Welcome Page:
titled: Tagline Practical Exam
Content:
-List-group containing the first two parts of the exam
-Palindrome checker checks the word and saves it on the database; displays the result in datatables.
-Number sorter: with the given array of numbers using Jquery and Javascript, created a function to sort the numbers in the array when the sort button is clicked.

##Register Page:
titled: Register Page
Content:
-register users using their full name; use their own provided email completed with their chosen password and secured by password confirmation.
-User is saved on the SQL Database while the passwords are encrypted.

##Login Page:
titled: Login Page
Content:
-validates the user's email and password to allow access to the application.
-locks the user using throttle after 3 max attempts

##Inventory Page:
titled: Inventory Page
Content:
-enables the user to add inventory specifically for his account
-shows the datatables for the stocks of inventory if the quantity of the items are greater than 0 it is shown as stock-in else if the quantity of the items are 
equal to 0 it is shown as stock-out.


^_^ -- Ivanvielle B. Dupaya
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
