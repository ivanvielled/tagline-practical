<?php

namespace App\Http\Controllers;

use App\Palindrome;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Session;

class PalindromeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            $palindromes = Palindrome::all();
            return view ('welcome', [
                'palindromes' => $palindromes
            ]);
        } else {
            $userId = Auth::user()->id;
            $user = User::where('id', $userId)->first();
            $palindromes = Palindrome::all();

            return view('welcome', [
                'users' => $user,
                'palindromes' => $palindromes
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $palindrome = new Palindrome;

        $result;

        $palindrome->word = $request->word;

        $revWord = strrev($palindrome->word);

        if ($palindrome->word == $revWord) {
            $result = 1;
        } else {
            $result = 0;
        }

        $palindrome->result = $result;

        $palindrome->save();

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Palindrome  $palindrome
     * @return \Illuminate\Http\Response
     */
    public function show(Palindrome $palindrome)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Palindrome  $palindrome
     * @return \Illuminate\Http\Response
     */
    public function edit(Palindrome $palindrome)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Palindrome  $palindrome
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Palindrome $palindrome)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Palindrome  $palindrome
     * @return \Illuminate\Http\Response
     */
    public function destroy(Palindrome $palindrome)
    {
        //
    }
}
