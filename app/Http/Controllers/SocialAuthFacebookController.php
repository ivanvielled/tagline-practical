<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Services\SocialFacebookAccountService;

class SocialAuthFacebookController extends Controller
{
    public function redirectFB()
    {
    	return Socialite::driver('facebook')->redirect();
    }

    public function callbackFB(SocialFacebookAccountService $service)
    {
    	$user = $service->createOrGetUserFB(Socialite::driver('facebook')->user());

    	    auth()->login($user);
    	    
    	return redirect('/');
    }
}
