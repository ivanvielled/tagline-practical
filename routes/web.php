<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Palindrome;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PalindromeController@index');

//check if string is palindrome
Route::post('/palindrome/check', 'PalindromeController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//get all items(stock-in)
Route::get('/inventory', 'ItemController@index');

//create new items
Route::get('/items/create', 'ItemController@create');
Route::post('/items/store', 'ItemController@store');

//buy items(stock-out)
Route::get('/stock-out/{id}', 'ItemController@edit');
Route::put('/stock-out/update/{id}', 'ItemController@update');

//Facebook Login
Route::get('/redirect-facebook', 'SocialAuthFacebookController@redirectFB');
Route::get('/callback-facebook', 'SocialAuthFacebookController@callbackFB');

//Google Login
Route::get('/redirect', 'SocialAuthGoogleController@redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback');
