<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Style -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
        <link data-require="datatables@1.10.12" data-semver="1.10.12" rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
        <link rel="stylesheet" type ="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

    </head>

    <body>

        @include('layouts.header')

        <main class="py-4">
            
            @yield('content')

        </main>

        @include('layouts.footer')

        <!-- Scripts -->
            <script src="{{ asset('js/jquery.slim.min.js') }}"></script>
            <script src="{{ asset('js/popper.min.js') }}"></script>
            <script src="{{ asset('js/bootstrap.min.js') }}"></script>
            <script src="Scripts/jquery-3.3.1.js"></script>
            <script data-require="jquery@3.0.0" data-semver="3.0.0" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.js"></script>
            <script data-require="datatables@1.10.12" data-semver="1.10.12" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
            <script data-require="datatables-responsive@*" data-semver="2.1.0" src="//cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js">
            </script>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#stockIn-datatable').DataTable({
                        responsive: true
                    });
                });

                $(document).ready(function() {
                    $('#stockOut-datatable').DataTable({
                        responsive: true
                    });
                });

                $(document).ready(function() {
                    $('#palindrome-datatable').DataTable({
                        responsive: true
                    });
                });

                let number1 = 1.5;
                let number2 = 2.3;
                let number3 = 5;
                let number4 = 32;
                let number5 = 45;

                let numArray = [number1, number2, number3, number4, number5];
                let sortedList = document.getElementById('sortedList');

                function sort() {
                    numArray.sort(function(a, b) {
                        return b - a
                    });

                    sortedList.innerText = numArray;
                }
            </script>
    </body>

</html>



