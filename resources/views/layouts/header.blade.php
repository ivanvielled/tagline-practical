<header class="container">
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a href="{{ url('/') }}" class="navbar-band">Tagline</a>

		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="navbar">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse text-right" id="navbar-main">
			<ul class="navbar-nav ml-auto">
				@guest
					<li class="nav-item">
						<a href="{{ url('/login') }}" class="nav-link">Login</a>
					</li>

					<li class="nav-item">
						<a href="{{ url('/register') }}" class="nav-link">Register</a>
					</li>
				@else
					<li class="nav-item">
						<a href="{{ url('/inventory') }}" class="nav-link">Inventory</a>
					</li>

					<li class="nav-item">
						<a onclick="document.querySelector('#logout-form').submit()" class="nav-link" href="#">Logout</a>
					</li>
				@endguest
			</ul>
		</div>
	</nav>
</header>

<form id="logout-form" class="d-none" action="{{ route('logout') }}" method="POST">
	@csrf
</form>