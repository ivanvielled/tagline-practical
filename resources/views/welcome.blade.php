@extends('layouts.app')

@section('title', 'Tagline Practical Exam')

@section('content')
	<div class="container">
		<div class="mt-5">
			<div class="row">
				<div class="col-md-12">
					<div class="list-group list-group-horizontal" id="list-tab" role="tablist">
						<a class="list-group-item list-group-item-action active" id="list-palindromes-list" data-toggle="list" href="#list-palindromes" role="tab" aria-controls="palindromes">Palindrome Checker</a>

						<a class="list-group-item list-group-item-action" id="list-numSort-list" data-toggle="list" href="#list-numSort" role="tab" aria-controls="numSort">Number Sorter</a>
					</div>
				</div>

				<div class="col-md-12">
					<div class="tab-content" id="tabContent">
						<div class="tab-pane fade show active" id="list-palindromes" role="tabpanel" aria-labelledby="palindromes">
							<form action="{{ url('palindrome/check') }}" method="post" enctype="multipart/form-data">
								@csrf
								<div class="form-group">
									<label>Palindrome Checker</label>
									<input class="form-control" type="text" name="word" required autocomplete="off">

									<button type="submit" class="btn btn-primary mt-3">Check</button>
								</div>
							</form>

							<table id="palindrome-datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>#ID</th>
										<th>Word</th>
										<th>Result</th>
									</tr>
								</thead>

								<tbody>
									@foreach($palindromes as $palindrome)
										<tr>
											<td>{{ $palindrome->id }}</td>
											<td>{{ $palindrome->word }}</td>
											@if ($palindrome->result === 1)
												<td>{{ $palindrome->word }} is a Palindrome.</td>
											@else
												<td>{{ $palindrome->word }} is not a Palindrome.</td>
											@endif
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>

						<div class="tab-pane fade show" id="list-numSort" role="tabpanel" aria-labelledby="numSort">
							<div class="row">
								<div class="col-md-6">
									<h5>Numbers to be sorted</h5><br>
									<div id="unsortedList" class="d-block">
										<span id="first">1.5</span>
										<span id="second">2.3</span>
										<span id="third">5</span>
										<span id="fourth">32</span>
										<span id="fifth">45</span>
									</div>

									<button class="btn btn-success" onclick="sort()">Sort</button>
								</div>

								<div class="col-md-6">
									<h5>Sorted Number List</h5>
									<div id="sortedList"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection