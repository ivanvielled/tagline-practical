@extends('layouts.app')

@section('title', 'Add Item')

@section('add-item-form')
	<form action="{{ url('items/store') }}" method="post" enctype="multipart/form-data">
		@csrf

		<div class="form-group">
			<label>Item Name</label>
			<input type="text" class="form-control" name="name" required autocomplete="off">
		</div>

		<div class="form-group">
			<label>Quantity</label>
			<input type="number" class="form-control" name="quantity" required autocomplete="off">
		</div>

		<button type="submit" class="btn btn-success">Save</button>
	</form>
@endsection

@section('content')
	<div class="container">
		<div class="card">
			<div class="card-header">Add Item</div>

			<div class="card-body">
				@yield('add-item-form')
			</div>
		</div>
	</div>
@endsection