@extends('layouts.app')

@section('title', 'Buy '.$items->name)

@section('buy-item-form')
	<form method="post" action='{{ url("/stock-out/update/$items->id") }}' enctype="multipart-form-data">
		@csrf
		@method("PUT")

		<div class="form-group">
			<label>Item Name</label>
			<input type="text" class="form-control" name="name" readonly value="{{ $items->name }}">
		</div>

		<div class="form-group">
			<label>Available Qty.</label>
			<input type="number" class="form-control" name="origQty" readonly value="{{ $items->quantity }}">
		</div>

		<div class="form-group">
			<label>Quantity</label>
			<input type="number" class="form-control" name="quantity" required autocomplete="off">
		</div>

		<button type="submit" class="btn btn-outline-success">Buy</button>
	</form>
@endsection

@section('content')
	<div class="container">
		<div class="card">
			<div class="card-header">Buy Item</div>

			<div class="card-body">
				@yield('buy-item-form')
			</div>
		</div>
	</div>
@endsection