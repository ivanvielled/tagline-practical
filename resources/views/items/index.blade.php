@extends('layouts.app')

@section('title', 'Inventory')

@section('content')
	<div class="container">
		<div class="text-right">
			<a href="{{ url('items/create') }}" class="btn btn-primary">Add Item</a>
		</div>

		<div class="row mt-5">
			<div class="col-md-4">
				<div class="list-group" id="list-tab" role="tablist">
					<a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-stockIn" role="tab" aria-controls="stock-in">Stock In</a>

					<a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href="#list-stockOut" role="tab" aria-controls="stock-out">Stock Out</a>
				</div>
			</div>

			<div class="col-md-8">
				<div class="tab-content" id="tabContent">
					<div class="tab-pane fade show active" id="list-stockIn" role="tabpanel" aria-labelledby="stock-in">
						<table id="stockIn-datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>#Item ID</th>
									<th>Item Name</th>
									<th>Quantity</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody>
								@foreach($items as $item)
									@if($item->quantity != 0)
									<tr>
										<td>{{ $item->id }}</td>
										<td>{{ $item->name }}</td>
										<td>{{ $item->quantity }}</td>
										<td>
											<a href='{{ url("/stock-out/$item->id") }}'>Buy</a>
										</td>
									</tr>
									@endif
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="tab-pane fade show" id="list-stockOut" role="tabpanel" aria-labelledby="stock-out">
						<table id="stockOut-datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>#Item ID</th>
									<th>Item Name</th>
									<th>Quantity</th>
								</tr>
							</thead>

							<tbody>
								@foreach($items as $item)
									@if($item->quantity === 0)
									<tr>
										<td>{{ $item->id }}</td>
										<td>{{ $item->name }} (not available)</td>
										<td>{{ $item->quantity }}</td>
									</tr>
									@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
@endsection